<?php
/**
 * NOTICE OF LICENSE.
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Ohm Conception
 *  @copyright 2020 Ohm Conception
 *  @license   license,txt
 */

use PrestaShop\PrestaShop\Adapter\Configuration as ConfigurationAdapter;
use PrestaShop\PrestaShop\Adapter\ContainerBuilder;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Presenter\Cart\CartPresenter;
use PrestaShop\PrestaShop\Adapter\Presenter\Object\ObjectPresenter;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Filesystem\Filesystem;

require_once(_PS_ROOT_DIR_.'/modules/noncegenerator/noncegenerator.php');

class FrontController extends FrontControllerCore
{
    public function __construct()
    {
        parent::__construct();
        $this->nonce = new NonceGenerator();
        $this->context->cookie->__set('cookie_nonce', $this->nonce->generateNonce());
    }

    public function registerJavascript($id, $relativePath, $params = array())
    {
        if (!is_array($params)) {
            $params = array();
        }
        if ($this->nonce->nonceEnabled()) {
            $default_params = [
                'position' => AbstractAssetManager::DEFAULT_JS_POSITION,
                'priority' => AbstractAssetManager::DEFAULT_PRIORITY,
                'inline' => false,
                'attributes' => 'nonce='.$this->context->cookie->cookie_nonce,
                'server' => 'local',
            ];
        } else {
            $default_params = [
                'position' => AbstractAssetManager::DEFAULT_JS_POSITION,
                'priority' => AbstractAssetManager::DEFAULT_PRIORITY,
                'inline' => false,
                'attributes' => '',
                'server' => 'local',
            ];
        }

        $params = array_merge($default_params, $params);
        $this->javascriptManager->register(
            $id,
            $relativePath,
            $params['position'],
            $params['priority'],
            $params['inline'],
            $params['attributes'],
            $params['server']
        );
    }
}
