<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once dirname(__FILE__).'../../../config/config.inc.php';
require_once dirname(__FILE__).'../../../init.php';

include_once('noncegenerator.php');
// require_once _PS_MODULE_DIR_.'ohmgift/classes/CustomGiftRules.php';

$noncegenerator = new Noncegenerator();
$securekey = Tools::getValue('securekey');

if ($noncegenerator->secure_key == $securekey) {
    switch (Tools::getValue('action')) {
        case 'ADD_NEW_CSP_DIRECTIVES':
            $res = Db::getInstance()->insert('ml_csp_directives', ['name' => Tools::getValue('data')]);
            if ($res) {
                $last_inserted_ID = Db::getInstance()->getRow('SELECT MAX(id) FROM '. _DB_PREFIX_ .'ml_csp_directives');
                echo Tools::jsonEncode([$last_inserted_ID['MAX(id)'], Tools::getValue('data')]);
            } else {
                echo false;
            }
            break;

        case 'REMOVE_CSP_DIRECTIVE':
            Db::getInstance()->delete('ml_csp_directives', 'id='.Tools::getValue('data'));
            break;

        case 'STATUS_RULES':
            $succ = $err = [];
            $err_ctr = 0;
            $list_id = Tools::jsonDecode(Tools::getValue('data'));

            if (count($list_id)) {
                foreach ($list_id as $data) {
                    if ($data->status == 'Enable') {
                        Db::getInstance()->update("ohmgiftrules", ['status'=>1], "id_ohmgiftrule = $data->id");
                    } else {
                        Db::getInstance()->update("ohmgiftrules", ['status'=>0], "id_ohmgiftrule = $data->id");
                    }
                }
            }
            break;
        case 'SINGLE_STATUS_RULES':
            $list_id = Tools::jsonDecode(Tools::getValue('data'));

            if (count($list_id)) {
                if ($list_id[0]->status == 'Enable') {
                    Db::getInstance()->update("ohmgiftrules", ['status'=>1], "id_ohmgiftrule = " . (int)$list_id[0]->id);
                } else {
                    Db::getInstance()->update("ohmgiftrules", ['status'=>0], "id_ohmgiftrule = " . (int)$list_id[0]->id);
                }
            }
            break;
        case 'FORM_SUBMIT':
            $name = Tools::getValue('name');
            $description = Tools::getValue('description');
            $id_product = Tools::getValue('id_product');
            $cart_amount = Tools::getValue('cart_amount');

            $result = $ohmgift->validateInput($name, $description, $id_product, $cart_amount);
            echo Tools::jsonEncode($result);
            break;
        default:
            exit;
    }
}
