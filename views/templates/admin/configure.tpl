{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="modal fade" id="modalNonce" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	    	<div class="modal-header text-center">
		        <h4 class="modal-title w-100 font-weight-bold">
		        	{l s='CSP Directives' mod='noncegenerator'}
		        </h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>

	     	<div class="modal-body mx-3">

		        <div class="flip-card">
				  <div class="flip-card-inner">
				    <div class="flip-card-front">
				        <div class="md-form mb-5">
					        <label data-error="wrong" data-success="right" for="defaultForm-email">Add new CSP directive's</label>
					        <input type="text" id="nonce_csp_dir" class="form-control validate">
				        </div>
				    </div>
				    <div class="flip-card-back">
				      	<table class="table table-hover table-striped tblCspDirectives">
			        		<thead>
			        			<tr>
			        				<th>
			        					<strong>{l s='CSP directive name' mod='noncegenerator'}</strong>
			        				</th>
			        				<th></th>
			        			</tr>
			        		</thead>
			        		<tbody>
			        			{foreach $csp_directives as $csp_dir}
				        			<tr>
				        				<td>{$csp_dir['name']|escape:'htmlall':'UTF-8'}</td>
				        				<td>
				        					<a href="" class="nonce-remove-directives" data-id="{$csp_dir['id']|escape:'htmlall':'UTF-8'}">
				        						<i class="process-icon-delete"></i>
				        					</a>
				        				</td>
				        			</tr>
			        			{/foreach}
			        		</tbody>
			        	</table>
				    </div>
				  </div>
				</div>

	      	</div>

	      	<div class="modal-footer d-flex justify-content-center">
	        	<button class="btn pull-left btn-default nonce-flip">{l s='View all CSP Directives' mod='noncegenerator'}</button>
	        	<button class="btn btn-primary btnAddNonce">{l s='Update list' mod='noncegenerator'}</button>
	      	</div>

	      	<div class="sg_loader-wrapper">
			   <div class="sg_circle-loader">
			       <div class="sg_checkmark draw"></div>
			   </div>
			</div>
	    </div>
  	</div>
</div>

{* <div class="panel">
	<h3><i class="icon icon-lock"></i> {l s='Information' mod='noncegenerator'}</h3>
	<p class="nonce-p">
		{l s='For more information about Content Security Policy, visit this ' mod='noncegenerator'}
        <a href="https://content-security-policy.com" target="_blank">link</a>.
		<br>
        Useful CSP Directive Reference link:
	</p>
	<ul>
		{foreach $csp_directives as $sl}
			<li>
				<a href="https://content-security-policy.com/{$sl|escape:'htmlall':'UTF-8'}" target="_blank">
					{$sl|escape:'htmlall':'UTF-8'}
				</a>
			</li>
		{/foreach}
	</ul>
</div> *}
