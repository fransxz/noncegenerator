/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$(function() {

	var $ajaxDirectivesBTN = $('<a>')
		.attr('href', '')
		.attr('class', 'btn btn-primary')
		.attr('data-toggle', 'modal')
		.attr('data-target', '#modalNonce')
		.text('Add CSP directive');

	$('.nonce-csp-directs')
		.closest('.form-group')
		.append($('<div class="col-lg-1">')
			.append($ajaxDirectivesBTN));

	$('.nonce-flip').click(function(e) {
		$('.flip-card-inner').toggleClass('flipped-card');
		$('.btnAddNonce').fadeToggle();
		e.preventDefault();
	});



	function stopLoader() {
        $('.sg_circle-loader').toggleClass('load-complete');
        $('.sg_checkmark').toggle();
        $('.sg_loader-wrapper').delay(2000).fadeOut('slow', function() {
            $('.sg_circle-loader').toggleClass('load-complete');
            $('.sg_checkmark').toggle();
        });
    }


	$('.nonce-remove-directives').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		$('.sg_loader-wrapper').fadeIn();
		param = {
            data : $(this).data('id'),
            securekey : securekey,
            action : 'REMOVE_CSP_DIRECTIVE',
        }
        
        $.post(ajax_url, param, function(data) {
        	$this.closest('tr').remove();
        	$('select.selectpicker option[value='+ $this.data('id') +']').remove();
        	stopLoader();
        });
	});

	$('.btnAddNonce').click(function(e) {
		$('.sg_loader-wrapper').fadeIn();
		param = {
            data : $('#nonce_csp_dir').val(),
            securekey : securekey,
            action : 'ADD_NEW_CSP_DIRECTIVES',
        }
        
        $.post(ajax_url, param, function(data) {

        	if (data) {
        		var x = $.parseJSON(data);

        		var obj = $('<tr>')
        			.append( $('<td>').text(x[1]) )
        			.append( $('<td>').html(
        				$('<a href="">')
        				.attr('class', 'nonce-remove-directives')
        				.attr('data-id', x[0])
        				.html($('<i class="process-icon-delete">'))
        				) 
        			);

        		$('.tblCspDirectives tbody').append(obj);

        		var data = {
				    id: x[0],
				    text: x[1]
				};

				var newOption = new Option(data.text, data.id, false, false);
				$('select.selectpicker').append(newOption).trigger('change');


        	} else {
        		alert('Error!');
        	}

        	stopLoader();
        });

	});




	$('.selectpicker').select2({
        width: "100%",
        // multiple: true,
        closeOnSelect: false,
    });

    cspDirectives($('select[name="NONCE_CSP_DIRECTIVES[]"]').val());

    $('select[name="NONCE_CSP_DIRECTIVES[]"]').change(function() {
    	cspDirectives($(this).val());
    });

    function cspDirectives(x) {
    	$('select[name=NONCE_DEFAULT_SRC_SOURCE_LIST]').closest('.form-group').fadeOut();
		$('select[name=NONCE_IMG_SRC_SOURCE_LIST]').closest('.form-group').fadeOut();
		$('select[name=NONCE_SCRIPT_SRC_SOURCE_LIST]').closest('.form-group').fadeOut();
		$('select[name=NONCE_STYLE_SRC_SOURCE_LIST]').closest('.form-group').fadeOut();

    	if (x) {
	    	x.forEach(function(i) {
	    		if (i == 0) {
	    			$('select[name=NONCE_DEFAULT_SRC_SOURCE_LIST]').closest('.form-group').fadeIn();
	    		}
	    		if (i == 1) {
	    			$('select[name=NONCE_IMG_SRC_SOURCE_LIST]').closest('.form-group').fadeIn();
	    		}
	    		if (i == 2) {
	    			$('select[name=NONCE_SCRIPT_SRC_SOURCE_LIST]').closest('.form-group').fadeIn();
	    		}
	    		if (i == 3) {
	    			$('select[name=NONCE_STYLE_SRC_SOURCE_LIST]').closest('.form-group').fadeIn();
	    		}
	    	});
	    }
    }


    function select_sl($this)
    {
    	if (parseInt($this.val()) == 5) {
    		$this.parent().next().fadeIn();
    	} else {
    		$this.parent().next().fadeOut();
    	}
    }

    // select_sl( $('.nonce-selects') );

    $('.nonce-selects').change(function() {
    	select_sl($(this));
    });



    $('.input-inline-element').each(function() {
	    var $clone = $(this).closest('div').clone().removeClass('col-lg-offset-3');
	    $(this).closest('.form-group').prev().append($clone).next().remove();
    });




	var btn = $('<button id="bt_gen_nonce" class="btn btn-primary">').text('Generate nonce');
	$('#NONCE_HASH_CODE').after(btn);

	$('#NONCE_HASH_CODE').parent().addClass('flex-container');

	$('#bt_gen_nonce').click(function(e) {
		e.preventDefault();
		$('#NONCE_HASH_CODE').val(generateId(40));
	});

	$('input[name=NONCE_STATUS]').change(function() {
		if ($(this).val()) {
			$('#NONCE_HASH_CODE').prop('disabled', true);
			$('#bt_gen_nonce').prop('disabled', true);
		} else {
			$('#NONCE_HASH_CODE').prop('disabled', false);
			$('#bt_gen_nonce').prop('disabled', false);
		}
	});

	if ($('input[name=NONCE_STATUS]:checked').val()) {
		$('#NONCE_HASH_CODE').prop('disabled', true);
		$('#bt_gen_nonce').prop('disabled', true);
	} else {
		$('#NONCE_HASH_CODE').prop('disabled', false);
		$('#bt_gen_nonce').prop('disabled', false);
	}


	// dec2hex :: Integer -> String
	// i.e. 0-255 -> '00'-'ff'
	function dec2hex (dec) {
		return dec < 10 ? '0' + String(dec) : dec.toString(16);
	}

	// generateId :: Integer -> String
	function generateId (len) {
		var arr = new Uint8Array((len || 40) / 2);
	  	window.crypto.getRandomValues(arr);
	  	return Array.from(arr, dec2hex).join('');
	}


	// If page reload, form data destroyed
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
});