<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    MLSS <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Adapter\Configuration as ConfigurationAdapter;
use Symfony\Component\Filesystem\Filesystem;

class Noncegenerator extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'noncegenerator';
        $this->tab = 'content_management';
        $this->version = '1.0.0';
        $this->author = 'MLSS';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Nonce Generator');
        $this->description = $this->l('Automatically generates nonces and inserts into script tags.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);

        $this->lang_selected = Configuration::get('PS_LANG_DEFAULT');

        $this->secure_key = Tools::encrypt($this->name);

        $this->csp_directives = array(
            'default-src',
            'img-src',
            'script-src',
            'style-src',
        );

        $this->source_lists = array(
            1 => 'none',
            2 => 'self',
            3 => 'unsafe-inline',
            4 => 'unsafe-eval',
            5 => 'Load domain',
        );

        
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('NONCE_ENABLED', true);
        Configuration::updateValue('NONCE_STATUS', true);
        Configuration::updateValue('NONCE_CSP_DIRECTIVES', '["2"]');
        Configuration::updateValue('NONCE_SCRIPT_SRC_SOURCE_LIST', 4);
        Configuration::updateValue('NONCE_HASH_CODE', hash('sha1', random_bytes(16), false));

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayHome');
    }

    public function uninstall()
    {
        Configuration::deleteByName('NONCE_ENABLED');
        Configuration::deleteByName('NONCE_STATUS');
        Configuration::deleteByName('NONCE_CSP_DIRECTIVES');

        Configuration::deleteByName('NONCE_DEFAULT_SRC_SOURCE_LIST');
        Configuration::deleteByName('NONCE_IMG_SRC_SOURCE_LIST');
        Configuration::deleteByName('NONCE_SCRIPT_SRC_SOURCE_LIST');
        Configuration::deleteByName('NONCE_STYLE_SRC_SOURCE_LIST');

        Configuration::deleteByName('NONCE_HASH_CODE');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    public function getCspDirectives()
    {
        return Db::getInstance()->executeS(
            'SELECT * FROM `' . _DB_PREFIX_ . 'ml_csp_directives`'
        );
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $output = '';
        if (((bool)Tools::isSubmit('submitNoncegeneratorModule')) == true) {
            $this->postProcess();
            $output .= $this->displayConfirmation($this->l('Nonce configured successfully.'));
        }
            
            echo '<pre style="display:none !important;">';
            print_r($this->getCspDirectives());
            echo '</pre>';
            

        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('source_lists', $this->source_lists);
        $this->context->smarty->assign('csp_directives', $this->getCspDirectives());

        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitNoncegeneratorModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        $final_options = [];
        foreach ($this->getCspDirectives() as $option) {
            $final_options[] = array(
                'id_option' => $option['id'],
                'name' => $option['name']
            );
        }

        $csp_d = array();
        $csp_d[] = array(
                'id_option' => null,
                'name' => ''
            );
        foreach ($this->source_lists as $i => $sl) {
            $csp_d[] = array(
                'id_option' => $i,
                'name' => $sl
            );
        }

        return array(
            'form' => array(
                'legend' => array(
                        'class' => 'moi',
                        'title' => $this->l('Settings'),
                        'icon' => 'icon-cogs',
                    ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'NONCE_ENABLED',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                        'desc' => ($this->l('If enabled, nonce will be activated upon accessing front.')),
                    ),
                    array(
                        'col' => 4,
                        'type' => 'select',
                        'multiple' => true,
                        'name' => 'NONCE_CSP_DIRECTIVES[]',
                        'label' => $this->l('CSP directives'),
                        'class' => 'selectpicker select2-picker nonce-csp-directs',
                        'options' => array(
                            'query' => $final_options,
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'col' => 2,
                        'type' => 'select',
                        'class' => 'nonce-selects',
                        'name' => 'NONCE_DEFAULT_SRC_SOURCE_LIST',
                        'label' => $this->l('Default-src source list'),
                        'options' => array(
                            'query' => $csp_d,
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'col' => 4,
                        'type' => 'text',
                        'class' => 'manual-nonce-input input-inline-element',
                        'name' => 'NONCE_DEFAULT_SRC_DOMAIN',
                        'placeholder' => 'Default src domain',
                        'desc' => $this->l('Use space separator if multiple domain.'),
                    ),
                    array(
                        'col' => 2,
                        'type' => 'select',
                        'class' => 'nonce-selects',
                        'name' => 'NONCE_IMG_SRC_SOURCE_LIST',
                        'label' => $this->l('Img-src source list'),
                        'options' => array(
                            'query' => $csp_d,
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'col' => 4,
                        'type' => 'text',
                        'class' => 'manual-nonce-input input-inline-element',
                        'name' => 'NONCE_IMG_SRC_DOMAIN',
                        'placeholder' => 'Img src domain',
                        'desc' => $this->l('Use space separator if multiple domain.'),
                    ),
                    array(
                        'col' => 2,
                        'type' => 'select',
                        'class' => 'nonce-selects',
                        'name' => 'NONCE_SCRIPT_SRC_SOURCE_LIST',
                        'label' => $this->l('Script-src source list'),
                        'options' => array(
                            'query' => $csp_d,
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'col' => 4,
                        'type' => 'text',
                        'class' => 'manual-nonce-input input-inline-element',
                        'name' => 'NONCE_SCRIPT_SRC_DOMAIN',
                        'placeholder' => 'Script src domain',
                        'desc' => $this->l('Use space separator if multiple domain.'),
                    ),
                    array(
                        'col' => 2,
                        'type' => 'select',
                        'class' => 'nonce-selects',
                        'name' => 'NONCE_STYLE_SRC_SOURCE_LIST',
                        'label' => $this->l('Style-src source list'),
                        'options' => array(
                            'query' => $csp_d,
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'col' => 4,
                        'type' => 'text',
                        'class' => 'manual-nonce-input input-inline-element',
                        'name' => 'NONCE_STYLE_SRC_DOMAIN',
                        'placeholder' => 'Style src domain',
                        'desc' => $this->l('Use space separator if multiple domain.'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Automatically generate nonce'),
                        'name' => 'NONCE_STATUS',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                        'desc' => ($this->l('If disabled, you will have to generate nonce manually.')),
                        'hint' => array(
                            $this->l(
                                'Nonce code will always generated everytime page in front refreshes.'
                            )
                        ),
                    ),
                    array(
                        'col' => 5,
                        'type' => 'text',
                        'class' => 'manual-nonce-input',
                        'name' => 'NONCE_HASH_CODE',
                        'label' => $this->l('Nonce code'),
                        'hint' => array($this->l(
                            'Generate nonce code either input manually or by button generated located in right.'
                        )),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues($input_value = false)
    {
        if ($input_value) {
            return array(
                'NONCE_ENABLED' => Tools::getValue('NONCE_ENABLED'),
                'NONCE_CSP_DIRECTIVES[]' => Tools::getValue('NONCE_CSP_DIRECTIVES'),
                
                'NONCE_DEFAULT_SRC_SOURCE_LIST' => Tools::getValue('NONCE_DEFAULT_SRC_SOURCE_LIST'),
                'NONCE_DEFAULT_SRC_DOMAIN' => Tools::getValue('NONCE_DEFAULT_SRC_DOMAIN'),
                
                'NONCE_IMG_SRC_SOURCE_LIST' => Tools::getValue('NONCE_IMG_SRC_SOURCE_LIST'),
                'NONCE_IMG_SRC_DOMAIN' => Tools::getValue('NONCE_IMG_SRC_DOMAIN'),
                
                'NONCE_SCRIPT_SRC_SOURCE_LIST' => Tools::getValue('NONCE_SCRIPT_SRC_SOURCE_LIST'),
                'NONCE_SCRIPT_SRC_DOMAIN' => Tools::getValue('NONCE_SCRIPT_SRC_DOMAIN'),
                
                'NONCE_STYLE_SRC_SOURCE_LIST' => Tools::getValue('NONCE_STYLE_SRC_SOURCE_LIST'),
                'NONCE_STYLE_SRC_DOMAIN' => Tools::getValue('NONCE_STYLE_SRC_DOMAIN'),
                
                'NONCE_STATUS' => Tools::getValue('NONCE_STATUS'),
                'NONCE_HASH_CODE' => Tools::getValue('NONCE_HASH_CODE'),
            );
        }

        return array(
            'NONCE_ENABLED' => Configuration::get('NONCE_ENABLED'),
            'NONCE_CSP_DIRECTIVES[]' => Tools::jsonDecode(Configuration::get('NONCE_CSP_DIRECTIVES')),
            
            'NONCE_DEFAULT_SRC_SOURCE_LIST' => Configuration::get('NONCE_DEFAULT_SRC_SOURCE_LIST'),
            'NONCE_DEFAULT_SRC_DOMAIN' => Configuration::get('NONCE_DEFAULT_SRC_DOMAIN'),
            
            'NONCE_IMG_SRC_SOURCE_LIST' => Configuration::get('NONCE_IMG_SRC_SOURCE_LIST'),
            'NONCE_IMG_SRC_DOMAIN' => Configuration::get('NONCE_IMG_SRC_DOMAIN'),
            
            'NONCE_SCRIPT_SRC_SOURCE_LIST' => Configuration::get('NONCE_SCRIPT_SRC_SOURCE_LIST'),
            'NONCE_SCRIPT_SRC_DOMAIN' => Configuration::get('NONCE_SCRIPT_SRC_DOMAIN'),
            
            'NONCE_STYLE_SRC_SOURCE_LIST' => Configuration::get('NONCE_STYLE_SRC_SOURCE_LIST'),
            'NONCE_STYLE_SRC_DOMAIN' => Configuration::get('NONCE_STYLE_SRC_DOMAIN'),
            
            'NONCE_STATUS' => Configuration::get('NONCE_STATUS'),
            'NONCE_HASH_CODE' => Configuration::get('NONCE_HASH_CODE'),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues(true);

        foreach (array_keys($form_values) as $key) {
            if (strpos($key, '[]')) {
                Configuration::updateValue(str_replace('[]', '', $key), Tools::jsonEncode(array_map("strip_tags", $form_values[$key])));
            } else {
                Configuration::updateValue($key, pSQL(Tools::getValue($key)));
            }
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        // if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJquery();
            $this->context->controller->addJqueryPlugin('select2');
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');

            $protocol = Configuration::get('PS_SSL_ENABLED') ? Tools::getHttpHost(true) : _PS_BASE_URL_;
            $this->smarty->assign('securekey', $this->secure_key);
            $this->smarty->assign('ajax_url', $protocol.$this->_path.'ajax.php');
            return $this->display(__FILE__, '/views/templates/admin/script.tpl');
        // }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJquery();
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');

        $metas = '';
        if (Configuration::get('NONCE_ENABLED') && isset($this->context->cookie->cookie_nonce)) {
            $csp_dir = Tools::jsonDecode(Configuration::get('NONCE_CSP_DIRECTIVES'));

            $nonce_default_src_source_list = Configuration::get('NONCE_DEFAULT_SRC_SOURCE_LIST');
            $nonce_img_src_source_list = Configuration::get('NONCE_IMG_SRC_SOURCE_LIST');
            $nonce_script_src_source_list = Configuration::get('NONCE_SCRIPT_SRC_SOURCE_LIST');
            $nonce_style_src_source_list = Configuration::get('NONCE_STYLE_SRC_SOURCE_LIST');
            
            $ctr = 0;
            $_csp = '';
            if (Configuration::get('NONCE_ENABLED')) {
                foreach ($csp_dir as $csp) {
                    $ctr++;
                    if ($csp == 0) {
                        if ($nonce_default_src_source_list) {
                            $_csp .= "default-src"." '".$this->source_lists[$nonce_default_src_source_list]."';";
                        } else {
                            $_csp .= "default-src" . " '";                            
                        }
                    } else if ($csp == 1) {
                        if ($nonce_img_src_source_list) {
                            $_csp .= "img-src"." '".$this->source_lists[$nonce_img_src_source_list]."';";    
                        } else {
                            $_csp .= "img-src"." '";
                        }
                    } else if ($csp == 2) {
                        if ($nonce_script_src_source_list) {
                            $_csp .= "script-src"." '".$this->source_lists[$nonce_script_src_source_list]."';";    
                        } else {
                            $_csp .= "script-src" . " '";
                        }
                    } else if ($csp == 3) {
                        if ($nonce_style_src_source_list) {
                            $_csp .= "style-src"." '".$this->source_lists[$nonce_style_src_source_list]. "';";
                        } else {
                            $_csp .= "style-src"." '";
                        }
                    }
                }
            }

            return '<meta http-equiv="Content-Security-Policy"
            content="'.Tools::substr($_csp, 0, -1).' \'nonce-'.$this->context->cookie->cookie_nonce.'\';">';
        }
    }

    public function nonceEnabled()
    {
        return Configuration::get('NONCE_ENABLED');
    }

    public function generateNonce()
    {
        if (Configuration::get('NONCE_STATUS')) {
            Configuration::updateValue('NONCE_HASH_CODE', hash('sha1', random_bytes(16), false), false, null, null);
        }
        return Configuration::get('NONCE_HASH_CODE');
    }
}
